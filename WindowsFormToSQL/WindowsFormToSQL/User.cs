﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MySql.Data.MySqlClient;
using System.Threading.Tasks;

namespace WindowsFormToSQL
{
    public class User
    {
        //Database stuff
        private const String SERVER = "127.0.0.1";
        private const String DATABASE = "test";
        private const String UID = "root";
        private const String PASSWORD = "password";
        private static MySqlConnection dbConn;

        //Local Variables
        private int id;
        private String username;
        private String password;

        //Getters & Setters
        public int Id { get => id; set => id = value; }
        public string Username { get => username; set => username = value; }
        public string Password { get => password; set => password = value; }

        //Constrcutor
        public User(int id, string username, string password)
        {
            Id = id;
            Username = username;
            Password = password;
        }
        //Initialising the Database
        public static void initializeDB()
        {
            MySqlConnectionStringBuilder builder = new MySqlConnectionStringBuilder();
            builder.Server = SERVER;
            builder.UserID = UID;
            builder.Password = PASSWORD;
            builder.Database = DATABASE;

            String connString = builder.ToString();

            builder = null;

            Console.WriteLine(connString);

            dbConn = new MySqlConnection(connString);
            
        }

        public static List<User> getUsers()
        {
            List<User> users = new List<User>();

            String query = "SELECT * FROM users";

            MySqlCommand cmd = new MySqlCommand(query, dbConn);

            dbConn.Open();

            MySqlDataReader reader = cmd.ExecuteReader();
            while(reader.Read())
            {
                int id = (int)reader["id"];
                String user = reader["username"].ToString();
                String pass = reader["pass_word"].ToString();

                User u = new User(id, user, pass);

                users.Add(u);
            }

            dbConn.Close();

            return users;
        }

        public static User Insert(String u,String p)
        {
            //String query = string.Format("INSERT INTO users(username,password) VALUES ('{0}','{1}')");
            String query = string.Format("INSERT INTO users(username, pass_word) VALUES ('{0}', '{1}')", u, p);

            MySqlCommand cmd = new MySqlCommand(query, dbConn);

            dbConn.Open();

            cmd.ExecuteNonQuery();
            int id = (int)cmd.LastInsertedId;
            User user = new User(id, u, p);

            dbConn.Close();

            return user;
        }

        public void update(string u,string p)
        {
            //String query = string.Format("INSERT INTO users(username, pass_word) VALUES ('{0}', '{1}')", u, p);
            String query = string.Format("UPDATE users SET username = '{0}',pass_word = '{1}' WHERE id = {2}", u, p, id);

            MySqlCommand cmd = new MySqlCommand(query, dbConn);

            dbConn.Open();

            cmd.ExecuteNonQuery();
            

            dbConn.Close();

        }

        public void delete()
        {
            String query = string.Format("DELETE FROM users WHERE id={0}", id);

            MySqlCommand cmd = new MySqlCommand(query, dbConn);

            dbConn.Open();

            cmd.ExecuteNonQuery();


            dbConn.Close();
        }
    }
}
