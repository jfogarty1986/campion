﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormToSQL
{
    public partial class Form1 : Form
    {

        private User currentUser;

        public Form1()
        {
            InitializeComponent();
            User.initializeDB();
        }

        private void Load()
        {
            List<User> users = User.getUsers();

            lvUsers.Items.Clear();
            foreach(User u in users)
            {
                ListViewItem item = new ListViewItem(new String[] {u.Id.ToString(),u.Username,u.Password});
                item.Tag = u;

                lvUsers.Items.Add(item);
            }
        }

        private void btnLoad_Click(object sender, EventArgs e)
        {
            Load();
        }

        private void btnInsert_Click(object sender, EventArgs e)
        {
            String u = txtUser.Text;
            String p = txtPassword.Text;
            
            if(String.IsNullOrEmpty(u) || String.IsNullOrEmpty(p))
            {
                MessageBox.Show("It's Empty");
                return;
            }

            currentUser = User.Insert(u, p);

            Load();
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            String u = txtUser.Text;
            String p = txtPassword.Text;

            if (String.IsNullOrEmpty(u) || String.IsNullOrEmpty(p))
            {
                MessageBox.Show("It's Empty");
                return;
            }

            currentUser.update(u, p);

            Load(); 
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if(currentUser == null)
            {
                MessageBox.Show("No User Selected");
                return;
            }

            currentUser.delete();

            Load();
        }

        private void lvUsers_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(lvUsers.SelectedItems.Count > 0)
            {
                ListViewItem item = lvUsers.SelectedItems[0];
                currentUser = (User)item.Tag;

                int id = currentUser.Id;
                String u = currentUser.Username;
                String p = currentUser.Password;

                txtID.Text = id.ToString();
                txtUser.Text = u;
                txtPassword.Text = p;
            }
            
        }
    }
}
