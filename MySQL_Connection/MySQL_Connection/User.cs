﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MySql.Data.MySqlClient;
using System.Threading.Tasks;

namespace MySQL_Connection
{
    class User
    {
        private const string SERVER = "127.0.0.1";
        private const string DATABASE = "test";
        private const string UID = "root";
        private const string PASSWORD = "password";
        private static MySqlConnection dbConn;
        private int id;
        private string username;
        private string password;

        public int Id { get => id; set => id = value; }
        public string Username { get => username; set => username = value; }
        public string Password { get => password; set => password = value; }

        public User(int id, string username, string password)
        {
            Id = id;
            Username = username;
            Password = password;
        }

        public static List<User> getUsers()
        {
            return null;
        }

        public void initializeDB()
        {
            MySqlConnectionStringBuilder builder = new MySqlConnectionStringBuilder();
            builder.Server = SERVER;
            builder.UserID = UID;
            builder.Database = DATABASE;
            builder.Password = PASSWORD;

            String connString = builder.ToString();

            builder = null;

            Console.WriteLine(connString);

            dbConn = new MySqlConnection(connString);


        }
    }
}
